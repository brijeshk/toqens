const functions = require('firebase-functions');

const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);
const cors = require('cors')({origin: true});


exports.notifyTokenChange = functions.database.ref('/queues/{queueId}/curTokens/{tokenNumber}').onWrite(event => {
  console.log('notifyTokenChange called');
  var snapshot = event.data;
  var payload = {
    notification: {
      title: 'ToQen',
      click_action: 'FCM_PLUGIN_ACTIVITY',
      icon: 'fcm_push_icon'
    },
    data: {
      queueId: event.params.queueId
    }
  };

  // only send a notification when a token has been called or expired
  if (!snapshot.exists() && snapshot.previous.exists()) {
    snapshot = snapshot.previous;
    payload.notification.body = 'Your token ' + event.params.tokenNumber + ' has expired.';
  } else if (snapshot.val().status == 'Called') {
    payload.notification.body = 'Your token ' + event.params.tokenNumber + ' is being called.';
  } else {
    console.log('uninteresting token status change');
    return;
  }

  console.log('sending notification to ' + snapshot.val().userPhone);
  return admin.database().ref('/users/' + snapshot.val().userPhone + '/notifyToken').once('value').then(notifyToken => {
    if (notifyToken.val()) {
      return admin.messaging().sendToDevice([notifyToken.val()], payload);
    }
    else {
      console.log('no notify token found for user');
      return null;
    }
  });
});

/*
exports.notifyExpiry = functions.database.ref('/queues/{queueId}/curTokens/{tokenNumber}').onDelete(event => {
  const snapshot = event.data.previous.val();
  const payload = {
    notification: {
      title: 'ToQen',
      body: 'Token ' + event.params.tokenNumber + ' has expired.',
      click_action: 'FCM_PLUGIN_ACTIVITY',
      icon: 'fcm_push_icon'
    },
    data: {
      queueId: event.params.queueId
    }
  };
  console.log('sending notification to ' + snapshot.userPhone);
  return admin.database().ref('/users/' + snapshot.userPhone + '/notifyToken').once('value').then(notifyToken => {
    if (notifyToken.val()) {
      return admin.messaging().sendToDevice([notifyToken.val()], payload);
    }
    else {
      console.log('no notify token found for user');
      return null;
    }
  });
  console.log('notifyExpiry called!!');
});
*/

exports.notifyStatusChange = functions.database.ref('/queues/{queueId}/curStats/statusMsg').onWrite(event => {
  console.log('in notifyStatusChange');
  const payload = {
    notification: {
      title: 'ToQen Queue Status',
      body: event.data.val(),
      click_action: 'FCM_PLUGIN_ACTIVITY',
      icon: 'fcm_push_icon'
    },
    data: {
      queueId: event.params.queueId
    }
  };
  return admin.database().ref('/queues/' + event.params.queueId + '/curTokens').once('value').then(tokens => {
    tokens.forEach((t) => {
      admin.database().ref('/users/' + t.val().userPhone + '/notifyToken').once('value').then(notifyToken => {
        if (notifyToken.val()) {
          return admin.messaging().sendToDevice([notifyToken.val()], payload);
        }
      });
    });
  });
});


exports.findQueues = functions.https.onRequest((req, res) => {
  cors(req, res, () => {
      const searchStr = req.query.text;
      console.log('finding queues with', searchStr);
      admin.database().ref('/queues').
        orderByChild('name').startAt(searchStr).endAt(searchStr+"\uf8ff").
        once("value").then((snapshot) => {
          var queueIds = [];
          snapshot.forEach((q) => {
              console.log("matched", q.key);
              queueIds.push(q.key);
          });
          res.status(200).send(queueIds);
      });
  });

});
