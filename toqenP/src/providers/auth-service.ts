import { Injectable } from '@angular/core';
import { AngularFireAuth, FirebaseAuthState, AuthProviders, AuthMethods } from 'angularfire2';
import { Storage } from '@ionic/storage';

@Injectable()
export class AuthService {
  private authState: FirebaseAuthState;
  public userName;
  public userPhone;

  constructor(public auth$: AngularFireAuth, public storage: Storage) {
    this.authState = auth$.getAuth();
    auth$.subscribe((state: FirebaseAuthState) => {
      this.authState = state;
    });
  }

  get authenticated(): boolean {
    return this.authState !== null;
  }
  
  signUp(email, password): firebase.Promise<FirebaseAuthState> {
    return this.auth$.createUser({
      email: email,
      password: password
    });
  }
  
  signIn(email, password): firebase.Promise<FirebaseAuthState> {
    return this.auth$.login({
      email: email,
      password: password
    },
    {
      provider: AuthProviders.Password,
      method: AuthMethods.Password,
    });
  }

  signOut(): void {
    this.auth$.logout();
    this.userName = '';
    this.userPhone = '';
    this.storage.remove('userInfo');
  }

  saveUserInfo(userName, userPhone) {
    this.userName = userName;
    this.userPhone = userPhone;
    this.storage.set('userInfo', { 'userName' : userName, 'userPhone': userPhone } );
  }

  readUserInfo() {
    return this.storage.get('userInfo').then((val) => {
      this.userName = val.userName;
      this.userPhone = val.userPhone;
    });
  }
}
