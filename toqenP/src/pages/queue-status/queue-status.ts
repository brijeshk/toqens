import { Component } from '@angular/core';
import {ViewChild} from '@angular/core';
import { NavController, NavParams, Navbar } from 'ionic-angular';
import { ActionSheetController } from 'ionic-angular';
import { AngularFire, FirebaseObjectObservable } from 'angularfire2';
import { CreateQueuePage } from '../create-queue/create-queue';
import { Utils } from '../../providers/utils';
import 'rxjs/add/operator/take';
import { AlertController } from 'ionic-angular';

@Component({
  selector: 'page-queue-status',
  template: `
  <ion-tabs style="font-size:14px">
    <ion-tab tabTitle="Status" [root]="tab1" [rootParams]="qid"></ion-tab>
    <ion-tab tabTitle="Tokens" [root]="tab2" [rootParams]="qid"></ion-tab>
  </ion-tabs>`
})
export class QueueStatusPage {
  tab1: any;
  tab2: any;
  tab3: any;
  qid;

  constructor(public navParams: NavParams) {
    this.qid = navParams.get('qid');
    console.log(this.qid);
    this.tab1 = QueueStatusTab;
    this.tab2 = QueueUsersTab;
    this.tab3 = CreateQueuePage;
  }
}

@Component({
  selector: 'page-queue-status',
  templateUrl: 'queue-status.html'
})
export class QueueStatusTab {
  q: FirebaseObjectObservable<any>;
  qid;
  @ViewChild(Navbar) navBar:Navbar;
  statusTxt;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public actionSheetCtrl: ActionSheetController,
              public alertCtrl: AlertController,
              public af: AngularFire,
              private _utils: Utils) {

    this.qid = navParams.data;
    this.q = af.database.object('/queues/' + this.qid);
    this.q.subscribe((obj) => {
      this.statusTxt = obj.curStats.statusMsg;
    });
  }

  ionViewDidLoad() {
    this.navBar.backButtonClick = (e:UIEvent) => {
        this.navCtrl.parent.viewCtrl.dismiss();
    };
  }

  editQueueInfo() {
    this.navCtrl.push(CreateQueuePage, { qid: this.qid });
  }

  /*showStatusMsgEditBox() {
    let prompt = this.alertCtrl.create({
      title: 'Set Status Message',
      message: "Token holders will be notified.",
      inputs: [
        {
          name: 'statusMsg',
          placeholder: ''
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {}
        },
        {
          text: 'Set',
          handler: data => {
            this.af.database.object('/queues/' + this.qid + '/curStats').update({'statusMsg' : data.statusMsg});
          }
        }
      ]
    });
    prompt.present();
  }*/

  setStatusMsg() {
    this.af.database.object('/queues/' + this.qid + '/curStats').update({'statusMsg' : this.statusTxt});
  }

  pauseQueue() {
    this.af.database.object('/queues/' + this.qid + '/curStats').update({'paused' : true});
  }

  resumeQueue() {
    this.af.database.object('/queues/' + this.qid + '/curStats').update({'paused' : false});
  }

  resetQueue() {
    var qref = this.q.$ref;
    qref.transaction((q) => {
      q.curStats.numTokensInQueue = 0;
      q.curStats.nowServingToken = 0;
      q.curStats.nextAvailableToken = 1;
      q.curTokens = {};
      return q;
    });
  }
}

@Component({
  selector: 'page-queue-status',
  template:`
  <ion-header>
    <ion-navbar color="orange" class="force-back-button">
      <ion-title>Queue</ion-title>
    </ion-navbar>
  </ion-header>
  <ion-content no-padding>
    <ion-list class="queue-list">
      <ion-item padding-left padding-right *ngFor="let e of ((q|async)?.curTokens|keys)">
        <ion-avatar item-left>
          <div class="circle">
            {{e.key}}
          </div>
        </ion-avatar>
        <h2>{{e.value.userName}}</h2>
        <p>{{e.value.status}}</p>
        <a style="margin-right:20px" (click)="$event.stopPropagation();" ion-button text-left button icon-only clear color="button" href="tel:{{e.value.userPhone}}" target="_system" item-right><ion-icon name="md-call"></ion-icon></a>
        <button style="margin-right:0px" (click)="showTokenActions(e);$event.stopPropagation();" ion-button text-right icon-only clear color="button" item-right><ion-icon name="md-more"></ion-icon></button>
      </ion-item>
    </ion-list>
  </ion-content>`
})
export class QueueUsersTab {
  q: FirebaseObjectObservable<any>;
  qid;
  @ViewChild(Navbar) navBar:Navbar;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public actionSheetCtrl: ActionSheetController,
              public af: AngularFire,
              private _utils: Utils) {
    this.qid = navParams.data;
    this.q = af.database.object('/queues/' + this.qid);
  }

  ionViewDidLoad() {
    this.navBar.backButtonClick = (e:UIEvent) => {
        this.navCtrl.parent.viewCtrl.dismiss();
    };
  }

  showTokenActions(t) {
    let actionSheet = this.actionSheetCtrl.create({
       title: 'Token Actions',
       buttons: [
         {
           text: 'Call Token',
           handler: () => {
              this.callToken(t);
           }
         },
         {
           text: 'Accept Token',
           handler: () => {
              this.acceptToken(t);
           }
         },
         {
           text: 'Expire Token',
           role: 'destructive',
           handler: () => {
              this.expireToken(t);
           }
         },
         {
           text: 'Cancel',
           role: 'cancel',
           handler: () => {}
         }
       ]
    });
    actionSheet.present();
  }

  callToken(t) {
    var tokenNumber = t.key;
    var qref = this.q.$ref;
    qref.transaction((q) => {
      if (q.curTokens[tokenNumber]) {
        q.curTokens[tokenNumber].status = 'Called';
      }
      return q;
    });
  }

  acceptToken(t) {
    var tokenNumber = t.key;
    var qref = this.q.$ref;
    qref.transaction((q) => {
      q.curStats.numTokensInQueue--;
      q.curStats.nowServingToken = tokenNumber;
      delete q.curTokens[tokenNumber];
      return q;
    });
  }

  expireToken(t) {
    var tokenNumber = t.key;
    var qref = this.q.$ref;
    qref.transaction((q) => {
      q.curStats.numTokensInQueue--;
      delete q.curTokens[tokenNumber];
      return q;
    });
  }
}
