import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AngularFire, FirebaseObjectObservable } from 'angularfire2';
import { AuthService } from '../../providers/auth-service';
import { Utils } from '../../providers/utils';
import * as moment from 'moment';

@Component({
  selector: 'page-create-queue',
  templateUrl: 'create-queue.html'
})
export class CreateQueuePage {
  form;
  q: FirebaseObjectObservable<any>;
  qid;
  queueName;
  facilityName;
  address;
  businessStartTime;
  businessEndTime;
  queueOpenTime;
  queueCloseTime;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public af: AngularFire,
              private _utils: Utils,
              private _auth: AuthService) {
      this.qid = navParams.get('qid');
      if (this.qid) {
        this.q = af.database.object('/queues/' + this.qid + '/info', { preserveSnapshot: true });
        this.q.subscribe(snapshot => {
          if (snapshot && snapshot.val()) {
            this.facilityName = snapshot.val().facility;
            this.queueName = snapshot.val().name;
            this.address = snapshot.val().address;
            this.businessStartTime = snapshot.val().businessStartTime;
            this.businessEndTime = snapshot.val().businessEndTime;
            this.queueOpenTime = snapshot.val().queueOpenTime;
            this.queueCloseTime = snapshot.val().queueCloseTime;
          }
        });
      }

      this.form = new FormGroup({
        facilityName: new FormControl("", Validators.required),
        queueName: new FormControl("", Validators.required),
        businessStartTime: new FormControl("", Validators.required),
        businessEndTime: new FormControl("", Validators.required),
        queueOpenTime: new FormControl("", Validators.required),
        queueCloseTime: new FormControl("", Validators.required),
        address: new FormControl("", Validators.required)
      });
  }

  saveQueue() {
      var t1 = moment(this.businessStartTime, "hh:mma");
      var t2 = moment(this.businessEndTime, "hh:mma");
      var t3 = moment(this.queueOpenTime, "hh:mma");
      var t4 = moment(this.queueCloseTime, "hh:mma");
      if (! t1.isValid() || ! t2.isValid() || ! t3.isValid() || ! t4.isValid()) {
        this._utils.toast('Please enter valid business and queue times: E.g. 9am, 5pm etc.');
        return;
      }

      var newQInfo = {
        facility: this.facilityName,
        name: this.queueName,
        address: this.address,
        businessStartTime: this.businessStartTime,
        businessEndTime: this.businessEndTime,
        queueOpenTime: this.queueOpenTime,
        queueCloseTime: this.queueCloseTime,
      }

      if (this.q) {
        // update existing queue info
        this.q.set(newQInfo);
        var newQUserInfo = {};
        newQUserInfo[this.qid] = this.facilityName + ' - ' + this.queueName;
        this.af.database.object('/users/' + this._auth.userPhone + '/q').update(newQUserInfo);
      }
      else {
        // create new queue
        // TODO: create a more unique queue id
        this.qid = 'q-' + Math.random().toString(36).substr(2, 16);
        var newQ = {
          curStats: {
            numTokensInQueue: 0,
            avgTimePerToken: 0,
            nowServingToken: 0,
            nextAvailableToken: 1
          },
          curTokens: {},
          info: newQInfo,
          srch1: this.facilityName.toLowerCase(),
          srch2: this.queueName.toLowerCase()
        };
        var newQUserInfo = {};
        newQUserInfo[this.qid] = this.facilityName + ' - ' + this.queueName;
        this.af.database.object('/queues/' + this.qid).set(newQ);
        this.af.database.object('/users/' + this._auth.userPhone + '/q').update(newQUserInfo);
      }

      this.navCtrl.pop();
  }
}
