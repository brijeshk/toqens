import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AngularFire, FirebaseListObservable } from 'angularfire2';
import { QueueStatusPage } from '../queue-status/queue-status';
import { CreateQueuePage } from '../create-queue/create-queue';
import { AuthService } from '../../providers/auth-service';

@Component({
  selector: 'page-myqueues',
  templateUrl: 'myqueues.html'
})
export class MyQueuesPage {
  items: FirebaseListObservable<any>;

  constructor(public navCtrl: NavController, public navParams: NavParams, public af: AngularFire, private _auth: AuthService) {
    this.items = af.database.list('/users/' + _auth.userPhone + '/q');
  }

  openQueueStatus(id) {
    this.navCtrl.push(QueueStatusPage, { qid: id });
  }

  createQueue() {
    this.navCtrl.push(CreateQueuePage);
  }

  deleteQueue(id) {

  }
}
