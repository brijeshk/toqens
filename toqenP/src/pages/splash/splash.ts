import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-splash',
  templateUrl: 'splash.html'
})
export class SplashPage {
  @ViewChild('animationImg') animationImg: ElementRef;

  splash = true;
  secondPage = LoginPage;

  constructor(public navCtrl: NavController) {

  }

  ionViewDidLoad() {
    let that = this;
    let animationEl = that.animationImg.nativeElement;

    setTimeout(() => {
      animationEl.src = "assets/img/splash.gif";
      setTimeout(() => {
        that.splash = false;
        that.navCtrl.setRoot(that.secondPage);
      }, 3000);
    }, 100);
  }

}
