import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyQueuesPage } from '../pages/myqueues/myqueues';
import { AdminPage } from '../pages/admin/admin';
import { LoginPage } from '../pages/login/login';
import { AuthService } from '../providers/auth-service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  pages: Array<{index: number, title: string, img: string, component: any}>;
  menuitem: number;

  constructor(public platform: Platform,
              public statusBar: StatusBar,
              public splashScreen: SplashScreen,
              public _auth: AuthService) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { index: 1,
        title: 'My Queues',
        img: 'menu_icon_queues.png',
        component: MyQueuesPage
      },
      { index: 2,
        title: 'My Profile',
        img: 'menu_icon_profile.png',
        component: AdminPage
      }
    ];
    this.menuitem = 1;
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      setTimeout(() => {
        this.splashScreen.hide();
      }, 100);
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.menuitem = page.index;
    this.nav.setRoot(page.component);
  }
  signOut() {
    this.menuitem = 1;
    this._auth.signOut();
    this.nav.setRoot(LoginPage);
  }
}
