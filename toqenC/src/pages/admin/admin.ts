import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service';

@Component({
  selector: 'page-admin',
  templateUrl: 'admin.html'
})
export class AdminPage {
  user = {
    name: '',
    phone: ''
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, public _auth: AuthService) {
    this.user.name = _auth.userName;
    this.user.phone = _auth.userPhone;
  }
}