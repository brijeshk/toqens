import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AngularFire, FirebaseListObservable } from 'angularfire2';
import { QueueStatusPage } from '../queue-status/queue-status';
import { SearchService } from '../../providers/search-service';
import { AuthService } from '../../providers/auth-service';

@Component({
  selector: 'page-favorites',
  templateUrl: 'favorites.html'
})
export class FavoritesPage {
  items: FirebaseListObservable<any>;
  items1: FirebaseListObservable<any>;
  items2: FirebaseListObservable<any>;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
              public af: AngularFire,
              private _auth: AuthService) {
    this.items = af.database.list('/users/' + _auth.userPhone + '/favorites');
  }

  openQueueStatus(id) {
    this.navCtrl.push(QueueStatusPage, { qid: id });
  }

  findQueues(ev: any) {
    let srch = ev.target.value;
    if (srch && srch.length > 2) {      
      srch = srch.toLowerCase();

      this.items1 = this.af.database.list('/queues', {
        query : {
          orderByChild: 'srch1',
          startAt: srch,
          endAt: srch + "\uf8ff",
          limitToFirst: 10
        }
      });

      this.items2 = this.af.database.list('/queues', {
        query : {
          orderByChild: 'srch2',
          startAt: srch,
          endAt: srch + "\uf8ff",
          limitToFirst: 10
        }
      });
    }
  }
}
