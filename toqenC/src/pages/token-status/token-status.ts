import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AngularFire, FirebaseObjectObservable } from 'angularfire2';
import { AlertController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service';
import { Utils } from '../../providers/utils';
import { MyTokensPage } from '../mytokens/mytokens';
import { QueueStatusPage } from '../queue-status/queue-status';
import * as moment from 'moment';

@Component({
  selector: 'page-token-status',
  templateUrl: 'token-status.html'
})
export class TokenStatusPage {
  q: FirebaseObjectObservable<any>;
  qsubs;
  token;
  eta;
  ampm;
  tokenPosition;
  uriAddr;

  constructor(public navCtrl: NavController, public navParams: NavParams, public af: AngularFire,
              private _auth: AuthService, private _utils: Utils,
              public alertCtrl: AlertController) {
    /*this.token = navParams.get('token');
    if (!this.token) {
      this._utils.toast('Invalid token. Please try with a valid token.');
      this.navCtrl.setRoot(MyTokensPage);
      return;
    }*/
    var qid = navParams.get('qid');
    var tokObj = this.af.database.object('/users/' + this._auth.userPhone + '/tokens/' + qid, { preserveSnapshot: true });
    tokObj.subscribe(snapshot => {
      this.token = snapshot.val();
    })

    this.q = af.database.object('/queues/' + qid);

    this.qsubs = this.q.subscribe((obj) => {
        if (obj.hasOwnProperty('$value') && !obj['$value']) {
          this._utils.toast('This queue does not exist anymore. Please check with the provider.');
        }
        else {
          this.tokenPosition = 0;
          var found = false;
          this.uriAddr = encodeURI(obj.info.address);

          if (obj.curTokens) {
            for (var tokenNumber in obj.curTokens) {
              this.tokenPosition++;
              if (tokenNumber == this.token.number) {
                var t = obj.curTokens[tokenNumber];
                if (t.userPhone == this._auth.userPhone) {
                  found = true;
                  this.token.status = t.status;
                  break;
                }
              }
            }
          }
          if (found) {
            if (this.token.status == 'Active') {
              var base = moment();
              var businessStartTime = moment(obj.info.businessStartTime, "hh:mma");
              if (base.isBefore(businessStartTime)) base = businessStartTime;
              var eta = new Date(base.toDate().getTime() + obj.curStats.avgTimePerToken * (this.tokenPosition-1) * 60000);
              var eta2 = eta.toLocaleTimeString();
              this.eta = eta2.slice(0, eta2.lastIndexOf(':'));
              this.ampm = eta2.split(' ')[1].toLowerCase();
            }
            else {
              this.eta = null;
              this.ampm = null;
            }
          }
          else {
            // mark token as expired
            this.token.status = 'Expired';
            this.af.database.object('/users/' + this._auth.userPhone + '/tokens/' + this.token.queueId).update({'status' : 'Expired'});
            this.eta = null;
            this.ampm = null;
          }
        }
    });
  }

  showDropConfirm() {
    let confirm = this.alertCtrl.create({
      title: 'Drop Token',
      message: 'Do you want to drop your token? This will lose your place in the queue.',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
          }
        },
        {
          text: 'Drop',
          handler: () => {
            this.dropToken();
          }
        }
      ]
    });
    confirm.present();
  }

  dropToken() {
    this.qsubs.unsubscribe();
    var qref = this.q.$ref;
    qref.transaction((q) => {
      if (q) {
          console.log('dropping token: ', this.token.number);
          q.curStats.numTokensInQueue--;
          delete q.curTokens[this.token.number];
      }
      return q;
    }, (error, committed, qsnapshot) => {
      if (error) {
        console.log('error in transaction', error);
        this._utils.toast('Unable to drop token. Please try again.');
        return;
      }
      else if (! committed) {
        this._utils.toast('Unable to drop token. Please try again.');
        return;
      }
      else if (! qsnapshot) {
        this._utils.toast('Unable to drop token. Please try again.');
        return;
      }

      this.af.database.list('/users/' + this._auth.userPhone + '/tokens').remove(this.token.queueId);
      this.navCtrl.setRoot(MyTokensPage);
    });
  }

  openQueuePage() {
    this.navCtrl.push(QueueStatusPage, { qid: this.token.queueId });
  }

  ngOnDestroy() {
    if (this.qsubs) this.qsubs.unsubscribe();
  }
}
