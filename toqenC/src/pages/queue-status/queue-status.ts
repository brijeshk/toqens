import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ActionSheetController } from 'ionic-angular';
import { AngularFire, FirebaseObjectObservable } from 'angularfire2';
import { Utils } from '../../providers/utils';
import { AuthService } from '../../providers/auth-service';
import { MyTokensPage } from '../mytokens/mytokens';
import 'rxjs/add/operator/take';
import * as moment from 'moment';

@Component({
  selector: 'page-queue-status',
  templateUrl: 'queue-status.html'
})
export class QueueStatusPage {
  q: FirebaseObjectObservable<any>;
  qid;
  qname;
  qaddress;
  eta;
  ampm;
  alreadyHoldsToken = false;
  isFavorite = false;
  isQueueOpen = false;
  subsc;
  uriAddr;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public actionSheetCtrl: ActionSheetController,
              public af: AngularFire,
              private _utils: Utils,
              private _auth: AuthService) {

    this.qid = navParams.get('qid');
    var tmpq = af.database.object('/queues/' + this.qid);

    // is this q a favorite?
    var fq = af.database.object('/users/' + this._auth.userPhone + '/favorites/' + this.qid, { preserveSnapshot: true }).take(1);
    fq.subscribe((snapshot) => {
      this.isFavorite = snapshot.exists();
    });

    this.subsc = tmpq.subscribe((obj) => {
      // does this q exist?
      if (obj.hasOwnProperty('$value') && !obj['$value']) {
        this._utils.toast('This queue does not exist anymore. Please check with the provider.');
        this.navCtrl.pop();
      }
      else {
        this.q = tmpq;
        this.qname = obj.info.facility + ' - ' + obj.info.name;
        this.qaddress = obj.info.address;
        this.uriAddr = encodeURI(obj.info.address);

        var nowTime = moment();

        if (obj.curStats) {
          var businessStartTime = moment(obj.info.businessStartTime, "hh:mma");
          var base = nowTime.isBefore(businessStartTime) ? businessStartTime : nowTime;
          var eta = new Date(base.toDate().getTime() + obj.curStats.avgTimePerToken * obj.curStats.numTokensInQueue * 60000);
          var eta2 = eta.toLocaleTimeString();
          this.eta = eta2.slice(0, eta2.lastIndexOf(':'));
          this.ampm = eta2.split(' ')[1].toLowerCase();
        }
        if (obj.curTokens) {
          for (var tokenNumber in obj.curTokens) {
            var t = obj.curTokens[tokenNumber];
            if (t.userPhone == this._auth.userPhone) {
              this.alreadyHoldsToken = true;
              break;
            }
          }
        }

        // is queue open right now for today?
        if (obj.curStats.paused) {
          this.isQueueOpen = false;
        }
        else {
          var qStartTime = moment(obj.info.queueOpenTime, "hh:mma");
          var qEndTime = moment(obj.info.queueCloseTime, "hh:mma");
          this.isQueueOpen = (nowTime.isAfter(qStartTime) && nowTime.isBefore(qEndTime));
        }
      }
    });
  }

  pickToken() {
    if (this.alreadyHoldsToken) {
      this._utils.toast('You already hold an active token in this queue!');
      return;
    }

    var qref = this.q.$ref;
    qref.transaction((q) => {
      if (q) {
          var p = q.curStats.nextAvailableToken;
          console.log('picking token: ', p);
          q.curStats.nextAvailableToken++;
          q.curStats.numTokensInQueue++;
          if (! q.curTokens) q.curTokens = {};
          q.curTokens[p] = { 'userPhone' : this._auth.userPhone, 'userName' : this._auth.userName, 'status' : 'Active' };
      }
      return q;
    }, (error, committed, qsnapshot) => {
      if (error) {
        console.log('error in transaction', error);
        this._utils.toast('Unable to pick token. Please try again.');
        return;
      }
      else if (! committed) {
        this._utils.toast('Unable to pick token. Please try again.');
        return;
      }
      else if (! qsnapshot) {
        this._utils.toast('Unable to pick token. Please try again.');
        return;
      }

      var q = qsnapshot.val();
      var p = q.curStats.nextAvailableToken - 1;
      console.log('picked token: ', p);
      var token = {
          'number': "" + p,
          'queueId': qsnapshot.key,
          'queueName': q.info.facility + ' - ' + q.info.name,
          //'date': q.curStats.date,
          'status': 'Active'
      }

      const tokens = this.af.database.list('/users/' + this._auth.userPhone + '/tokens');
      tokens.update(token.queueId, token);
      this._utils.toast('You picked token # ' + p + '. Added to My Tokens.');
      this.navCtrl.setRoot(MyTokensPage);
    });
  }

  faveQueue() {
    if (this.isFavorite) {
      this.af.database.object('/users/' + this._auth.userPhone + '/favorites/' + this.qid).remove();
      this.isFavorite = false;
    }
    else {
      var fq = { 'qname' : this.qname, 'qaddress' : this.qaddress };
      this.af.database.object('/users/' + this._auth.userPhone + '/favorites/' + this.qid).set(fq);
      this.isFavorite = true;
    }
  }

  ngOnDestroy() {
    if (this.subsc) this.subsc.unsubscribe();
  }
}
