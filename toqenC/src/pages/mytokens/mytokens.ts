import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AngularFire, FirebaseListObservable } from 'angularfire2';
import { Utils } from '../../providers/utils';
import { AuthService } from '../../providers/auth-service';
import { TokenStatusPage } from '../token-status/token-status';
import { FavoritesPage } from '../favorites/favorites';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-mytokens',
  templateUrl: 'mytokens.html'
})
export class MyTokensPage {
  items1: FirebaseListObservable<any>;
  items2: FirebaseListObservable<any>;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private _utils: Utils,
              public af: AngularFire,
              private _auth: AuthService) {
    if (! _auth.userPhone) {
      this._utils.toast('No user logged in!');
      this.navCtrl.setRoot(LoginPage);
      return;
    }

    this.items1 = af.database.list('/users/' + _auth.userPhone + '/tokens', {
      query: {
        orderByChild: 'status',
        equalTo: 'Active'
      }
    });

    this.items2 = af.database.list('/users/' + _auth.userPhone + '/tokens', {
      query: {
        orderByChild: 'status',
        equalTo: 'Expired'
      }
    });

    if (_utils.openWithQueueId) {
      this.navCtrl.push(TokenStatusPage, { qid: _utils.openWithQueueId });
      _utils.openWithQueueId = null;
    }
  }

  openTokenStatus(t) {
    this.navCtrl.push(TokenStatusPage, { qid: t.queueId });
  }

  openQueuesPage() {
    this.navCtrl.setRoot(FavoritesPage);
  }
}
