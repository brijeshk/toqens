import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AngularFire } from 'angularfire2';
import { MyTokensPage } from '../mytokens/mytokens';
import { AuthService } from '../../providers/auth-service';
import { Utils } from '../../providers/utils';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  form;
  public loading;

  constructor(public loadingCtrl: LoadingController, public navCtrl: NavController, public navParams: NavParams, private _auth: AuthService,
              private _utils: Utils, public af: AngularFire) {
    this.loading = loadingCtrl.create();
    this.form = new FormGroup({
      userName: new FormControl("", Validators.required),
      userPhone: new FormControl("", Validators.required)
    });

    _auth.readUserInfo().then(() => {
      console.log('signing in with saved credentials: ', _auth.userPhone);
      this.form.value.userName = _auth.userName;
      this.form.value.userPhone = _auth.userPhone;
      this.signIn();
    }).catch((error) => {
      console.log("no saved credentials");
    });
  }

  signIn() {
    this.loading.present();

    var email = this.form.value.userPhone + '@toqens.com';
    var password = 'UseImeiOfPhone';
    this._auth.signIn(email, password)
    .then(() => {
      this.loading.dismiss();
      this._auth.saveUserInfo(this.form.value.userName, this.form.value.userPhone);
      this._auth.saveNotifyToken();
      this.navCtrl.setRoot(MyTokensPage);
    })
    .catch((error) => {
      this.loading.dismiss();
      this._utils.toast('Error signing in. Please enter a registered phone number, or sign up with a new number.');
      console.log("auth failed: " + JSON.stringify(error));
    });
  }

  signUp() {
    this.loading.present();

    var email = this.form.value.userPhone + '@toqens.com';
    var password = 'UseImeiOfPhone';
    this._auth.signUp(email, password)
    .then(() => {
      this.loading.dismiss();
      this._auth.saveUserInfo(this.form.value.userName, this.form.value.userPhone);
      this.navCtrl.setRoot(MyTokensPage);
    })
    .catch((error) => {
      this.loading.dismiss();
      this._utils.toast('Error registering. This phone number may be already registered.');
      console.log("auth failed: " + JSON.stringify(error));
    });
  }

  signInTestUser() {
    this.form.value.userName = 'Test User';
    this.form.value.userPhone = '1234567890';
    this.signIn();
  }

}
