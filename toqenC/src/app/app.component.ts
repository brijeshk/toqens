import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyTokensPage } from '../pages/mytokens/mytokens';
import { FavoritesPage } from '../pages/favorites/favorites';
import { AdminPage } from '../pages/admin/admin';
import { LoginPage } from '../pages/login/login';
import { AuthService } from '../providers/auth-service';
import { Utils } from '../providers/utils';

declare var FCMPlugin;

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  pages: Array<{index: number, title: string, img: string, component: any}>;
  menuitem: number;

  constructor(public platform: Platform,
              public statusBar: StatusBar,
              public splashScreen: SplashScreen,
              public _auth: AuthService,
              public _utils: Utils) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { index: 1,
        title: 'My Tokens',
        img: 'menu_icon_feed.png',
        component: MyTokensPage
      },
      { index: 2,
        title: 'Queues',
        img: 'menu_icon_queues.png',
        component: FavoritesPage
      },
      { index: 3,
        title: 'My Profile',
        img: 'menu_icon_profile.png',
        component: AdminPage
      }
    ];
    this.menuitem = 1;
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      setTimeout(() => {
        this.splashScreen.hide();
      }, 100);

      if (this.platform.is('cordova')) {
        var that = this;
        if (FCMPlugin) {
          FCMPlugin.onNotification(function(data) {
            if (data.wasTapped) {
              if (data.queueId) {
                that._utils.openWithQueueId = data.queueId;
              }
            }
          });
        }
      }
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.menuitem = page.index;
    this.nav.setRoot(page.component);
  }

  signOut() {
    this.menuitem = 1;
    this._auth.signOut();
    this.nav.setRoot(LoginPage);
  }
}
