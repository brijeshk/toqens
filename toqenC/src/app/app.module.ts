import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MyApp } from './app.component';
import { MyTokensPage } from '../pages/mytokens/mytokens';
import { FavoritesPage } from '../pages/favorites/favorites';
import { QueueStatusPage } from '../pages/queue-status/queue-status';
import { TokenStatusPage } from '../pages/token-status/token-status';
import { FindQueuePage } from '../pages/find-queue/find-queue';
import { AdminPage } from '../pages/admin/admin';
import { LoginPage } from '../pages/login/login';
import { SplashPage } from '../pages/splash/splash';
import { AngularFireModule } from 'angularfire2';
import { AuthService } from '../providers/auth-service';
import { SearchService } from '../providers/search-service';
import { Utils } from '../providers/utils';
import { KeysPipe } from '../pipes/keyspipe';
import { IonicStorageModule } from '@ionic/storage';

export const firebaseConfig = {
  apiKey: "AIzaSyDw0dNM51nI6tKT1l3TL2nxe_0x-aCJ2-Q",
  authDomain: "toqen-1a30c.firebaseapp.com",
  databaseURL: "https://toqen-1a30c.firebaseio.com",
  storageBucket: "toqen-1a30c.appspot.com",
  messagingSenderId: "67234860163"
};

@NgModule({
  declarations: [
    MyApp,
    MyTokensPage,
    FavoritesPage,
    QueueStatusPage,
    TokenStatusPage,
    FindQueuePage,
    AdminPage,
    LoginPage,
    SplashPage,
    KeysPipe
  ],
  imports: [
    //IonicModule.forRoot(MyApp),
    IonicModule.forRoot(MyApp, {}, {//navigate using URLs
      links: [
        { component: MyTokensPage, name: 'MyTokens', segment: 'mytokens' },
        { component: FavoritesPage, name: 'Favorites', segment: 'favorites' },
        { component: QueueStatusPage, name: 'QueueStatus', segment: 'queuestatus' },
        { component: TokenStatusPage, name: 'TokenStatus', segment: 'tokenstatus' },
        { component: FindQueuePage, name: 'FindQueue', segment: 'findqueue' },
        { component: AdminPage, name: 'Admin', segment: 'admin' },
        { component: LoginPage, name: 'Login', segment: 'login' }
      ]
    }),
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MyTokensPage,
    FavoritesPage,
    QueueStatusPage,
    TokenStatusPage,
    FindQueuePage,
    AdminPage,
    LoginPage,
    SplashPage
  ],
  providers: [
    SplashScreen,
    StatusBar,
    AuthService,
    SearchService,
    Utils,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
