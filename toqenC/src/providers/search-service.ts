import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class SearchService {
  static get parameters() {
      return [[Http]];
  }

  constructor(public http: Http) {
  }

  findQueues(query) {
    var url = 'https://us-central1-toqen-1a30c.cloudfunctions.net/findQueues?text=' + encodeURI(query);
    var response = this.http.get(url).map(res => res.json());
    return response;
  }
}
