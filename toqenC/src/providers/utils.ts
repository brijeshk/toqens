import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';

@Injectable()
export class Utils {
  // global to hold queueId if opened from notification tap
  openWithQueueId;

  constructor(public toastCtrl: ToastController) {    
  }

  toast(msg) {
    let t = this.toastCtrl.create({
        message: msg,
        duration: 3000,
        position: 'bottom'
    });
    t.present();
  }
}
