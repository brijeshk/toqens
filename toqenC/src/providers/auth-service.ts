import { Inject, Injectable } from "@angular/core";
import { AngularFireAuth, FirebaseAuthState, AuthProviders, AuthMethods, FirebaseApp, AngularFire } from 'angularfire2';
import * as firebase from 'firebase';
import { Storage } from '@ionic/storage';
import { Platform } from 'ionic-angular';

declare var FCMPlugin;

@Injectable()
export class AuthService {
  private authState: FirebaseAuthState;
  private _messaging: firebase.messaging.Messaging;
  public userName;
  public userPhone;

  constructor(public auth$: AngularFireAuth, public storage: Storage,
              public af: AngularFire,
              public platform: Platform,
              @Inject(FirebaseApp) private _firebaseApp: firebase.app.App) {
    this._messaging = firebase.messaging(this._firebaseApp);
    this.authState = auth$.getAuth();
    auth$.subscribe((state: FirebaseAuthState) => {
      this.authState = state;
    });
  }

  get authenticated(): boolean {
    return this.authState !== null;
  }

  signUp(email, password): firebase.Promise<FirebaseAuthState> {
    return this.auth$.createUser({
      email: email,
      password: password
    });
  }

  signIn(email, password): firebase.Promise<FirebaseAuthState> {
    return this.auth$.login({
      email: email,
      password: password
    },
    {
      provider: AuthProviders.Password,
      method: AuthMethods.Password,
    });
  }

  signOut(): void {
    this.auth$.logout();
    this.userName = '';
    this.userPhone = '';
    this.storage.remove('userInfo');
  }

  saveUserInfo(userName, userPhone) {
    this.userName = userName;
    this.userPhone = userPhone;
    this.storage.set('userInfo', { 'userName' : userName, 'userPhone': userPhone } );
  }

  readUserInfo() {
    return this.storage.get('userInfo').then((val) => {
      this.userName = val.userName;
      this.userPhone = val.userPhone;
    });
  }

  saveNotifyToken() {
    if (this.platform.is('cordova')) {
      try {
        if (FCMPlugin && FCMPlugin.getToken) {
          FCMPlugin.getToken((token) => {
            console.log('Got notify token:', token);
            this.af.database.object('/users/' + this.userPhone).update({'notifyToken': token});
          });
        }
      } catch (e) {
        console.log('Error', e);
      }
    }
    else {
      this._messaging.getToken().then((currentToken) => {
        if (currentToken) {
          console.log('Got notify token:', currentToken);
          this.af.database.object('/users/' + this.userPhone).update({'notifyToken': currentToken});
        } else {
          this.requestNotificationsPermissions();
        }
      }).catch((error) => {
        console.error('Unable to get messaging token:', error);
      });
    }
  }

  requestNotificationsPermissions() {
    this._messaging.requestPermission().then(() => {
      this.saveNotifyToken();
    }).catch((error) => {
      console.error('Unable to get permission to notify:', error);
    });
  }
}
